package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.Org;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统机构表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-11-07
 */
public interface OrgMapper extends BaseMapper<Org> {

}
