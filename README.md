# crab

#### 项目介绍
crab 河蟹，极速 Web 开发框架。

- 允许商用不允许二次开源

- 演示地址：http://crab.kuaituima.com

- 购买 Spring Cloud 企业版联系 jobob@qq.com

#### 启动说明
- Idea 安装 lombok 设置动态编译【不会百度】
- Gradle 4+ 安装环境变量配置【不会百度】
- 导入 sql 修改 service-system 对应 yml 配置
- 导入 crab 运行 SystemApplication.java 点击控制台访问目录
- 关注 sql 文件夹 cxy.png 公众号回复 crab 免费获取授权码


# 程序猿福利
[【全民云计算】云主机低至2折](https://promotion.aliyun.com/ntms/act/qwbk.html?userCode=5wbjwd1y)


# Crab 效果图

![1](https://images.gitee.com/uploads/images/2018/1225/132052_0617f385_12260.png "1.png")
![2](https://images.gitee.com/uploads/images/2018/1225/132102_5c11be57_12260.png "2.png")
![3](https://images.gitee.com/uploads/images/2018/1225/132111_944c5cdd_12260.png "3.png")
![4](https://images.gitee.com/uploads/images/2018/1225/132119_f438422a_12260.png "4.png")
![5](https://images.gitee.com/uploads/images/2018/1225/132128_1d5f0bc7_12260.png "5.png")
![6](https://images.gitee.com/uploads/images/2018/1225/132136_d64f529e_12260.png "6.png")
